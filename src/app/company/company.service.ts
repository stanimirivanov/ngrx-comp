import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

import { Company } from './company';

@Injectable()
export class CompanyService {

  companiesCollection: AngularFirestoreCollection<Company>;
  companies: Observable<Company[]>;
  companyDoc: AngularFirestoreDocument<Company>;

  constructor(private readonly afs: AngularFirestore) {
    this.companiesCollection = this.afs.collection('companies');

    this.companies = this.companiesCollection.snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Company;
          const id = data.id = a.payload.doc.id;
          return { id, ...data };
        });
    });
  }

  getCompanies(): Observable<Company[]> {
    return this.companies;
  }

  getCompany(id: string): Observable<Company> {
    return this.afs.doc<Company>(`companies/${id}`).snapshotChanges()
      .map(action => {
        const data = action.payload.data() as Company;
        const id = action.payload.id;
        return { id, ...data };
      });
  }

  createCompany(company: Company): Promise<any> {
    const id = this.afs.createId();
    return this.companiesCollection.add({ id, ...company });
  }

  updateCompany(company: Company): Promise<void> {
    this.companyDoc = this.afs.doc<Company>(`companies/${company.id}`);
    return this.companyDoc.update(company);
  }

  removeCompany(company: Company): Promise<void> {
    this.companyDoc = this.afs.doc<Company>(`companies/${company.id}`);
    return this.companyDoc.delete();
  }

}

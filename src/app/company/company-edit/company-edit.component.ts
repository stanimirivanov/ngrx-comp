import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { CompanyService } from '../company.service';
import { Company } from '../company';

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss']
})
export class CompanyEditComponent implements OnInit {

  isNewCompany: boolean;
  companyId: string;
  company$: Observable<Company>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private companyService: CompanyService
  ) { }

  ngOnInit() {
    this.companyId = this.activatedRoute.snapshot.params['id'];
    this.isNewCompany = this.companyId === 'new';
    if(this.isNewCompany) {
      this.company$ = Observable.of({}) as Observable<Company>;
    } else {
      this.company$ = this.companyService.getCompany(this.companyId);
    }
  }

  saveCompany(company) {
    const save = this.isNewCompany
      ? this.companyService.createCompany(company)
      : this.companyService.updateCompany(company);

    save.then(_ => this.router.navigate([`company-list`]));
  }

  removeCompany(company) {
    this.companyService.removeCompany(company)
      .then(_ => this.router.navigate([`company-list`]));
  }

}
